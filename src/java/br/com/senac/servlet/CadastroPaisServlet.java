/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.banco.CidadeDAO;
import br.com.senac.banco.Conexao;
import br.com.senac.banco.PaisDAO;
import br.com.senac.modelo.Pais;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrador
 */
public class CadastroPaisServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //perga o que veio de paramentro
        int codigo = Integer.parseInt(request.getParameter("codigo"));
        String nome = request.getParameter("nome");

        HttpSession session = request.getSession();

        Pais p = (Pais) session.getAttribute("pais");

        // criar um objeto 
        Pais pais = new Pais();
        pais.setCodigo(codigo);
        pais.setNome(nome);

        //salvar objeto
        PaisDAO dao = new PaisDAO();

        if (pais.getCodigo() == 0) {
            dao.salvar(pais);
        } else {
            dao.atualizar(pais);
        }

        // response.sendRedirect("./lista.jsp");
        RequestDispatcher dispatcher = request.getRequestDispatcher("./lista.jsp");

        dispatcher.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

       

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
