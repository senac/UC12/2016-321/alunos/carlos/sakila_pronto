<%@page import="br.com.senac.modelo.Cidade"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<%@page import="java.util.List"%>


<jsp:include page="../header.jsp" />

<%
    CidadeDAO dao = new CidadeDAO();
    List<Cidade> lista = dao.listarTodos();

%>


<div class="container">
    <fieldset>
        <legend>Lista de Cidades</legend> 

        <table class="table table-hover">
            <thead>
                <tr>
                    <td>C�digo</td><td>Nome </td><td>Pais</td>
                </tr>
            </thead>
            <tbody>
                <% for (Cidade c : lista) {%>
                <tr>
                    
                    <td><%= c.getCodigo()%></td>
                    <td><%= c.getNome()%> </td>
                    <td><%= c.getPais()%> </td>
                    
                </tr>

                <%}%>
            </tbody>
        </table>
    </fieldset>
</div>


<jsp:include page="../footer.jsp" />
